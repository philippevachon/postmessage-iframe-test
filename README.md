# Test de communication entre un iFrame et son parent avec postMessage

## Émetteur 
L'émetteur est une page html qui est destinée à être intégrée dans un iframe dans une page réceptrice. L'émetteur utilise `postMessage` pour envoyer un message au récepteur à toutes les secondes. 

L'émetteur devine l'url du récepteur avant d'envoyer le message : 
``` javascript
var url = (window.location != window.parent.location) ? document.referrer: document.location;
parent.postMessage("Hello", url);
```

## Récepteur
Cette page intègre l'émetteur dans un iFrame et reçoit ses messages. Le message "hello" est logger dans la console du fureteur à toutes les secondes. 



